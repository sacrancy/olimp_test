// generated on 2016-02-08 using generator-cyber 0.1.0
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const autoprefixer = require('autoprefixer');
const assets = require('postcss-assets');
const browserSync = require('browser-sync');
const del = require('del');
const sprites = require('postcss-sprites').default;
const spritesUpdateRule = require('postcss-sprites').updateRule;
const postcss = require('postcss');
const cssnano = require('cssnano');
const pxtorem = require('postcss-pxtorem');
const selectorMatches = require('postcss-selector-matches');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const PATHS = {
  build: 'dist',
  temp: '.tmp',
  src: 'app'
};
// **********************************************************************

gulp.task('views', () => {
  return gulp.src([PATHS.src +'/views/*.html',PATHS.src +'/components/**/*.html'])
    .pipe($.nunjucksRender({path: [PATHS.src +'/views',PATHS.src +'/components']}))
    .pipe(gulp.dest( PATHS.temp ));
});

gulp.task('styles', () => {
  return gulp.src( PATHS.src +'/styles/*.scss')
    .pipe($.plumber({errorHandler: $.notify.onError("Error: <%= error.message %>")}))
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.'],
    }).on('error', $.sass.logError))
    .pipe($.postcss([
      assets({
        basepPath: PATHS.src +'/',
        cachebuster: true,
        loadPaths: [ PATHS.src +'/images/'],
      }),
      selectorMatches(),
      pxtorem({
        propWhiteList: [
          'font', 'font-size', 'line-height',
          'margin', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left',
          'padding', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left',
          'left', 'right', 'top', 'bottom',
          'width', 'height', 'min-width', 'max-width', 'max-height', 'min-height',
          'border-radius', 'border-top-left-radius', 'border-top-right-radius', 'border-bottom-left-radius', 'border-bottom-right-radius',
          'border-width', 'border-left-width', 'border-right-width', 'border-top-width', 'border-bottom-width', 'border'
        ],
      }),
      autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}),
    ]))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest( PATHS.temp +'/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src( PATHS.src +'/scripts/**/*.js')
    .pipe($.plumber({errorHandler: $.notify.onError("Error: <%= error.message %>")}))
    .pipe($.sourcemaps.init())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest( PATHS.temp +'/scripts'))
    .pipe(reload({stream: true}));
});

gulp.task('html', ['views', 'styles', 'scripts'], () => {
  return gulp.src( PATHS.temp +'/*.html')
    .pipe($.prettify({indent_size: 2, eol: '\r\n'}))
    .pipe($.useref({
      searchPath: [ PATHS.temp, PATHS.src, '.'],
      noAssets: false
    }))
    .pipe(gulp.dest( PATHS.build ));
});

gulp.task('images', () => {
  return gulp.src(PATHS.src +'/images/*.*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest( PATHS.build +'/images'));
});

gulp.task('samples', () => {
  return gulp.src(PATHS.src +'/images/samples/*.*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest( PATHS.build +'/images/samples'));
});

gulp.task('fonts', () => {
  return gulp.src( PATHS.src +'/fonts/**/*')
    .pipe(gulp.dest( PATHS.build +'/fonts'));
});

gulp.task('extras', () => {
  return gulp.src([
    PATHS.src +'/*.*',
    '!'+ PATHS.src +'/*.html',
  ], {
    dot: true
  }).pipe(gulp.dest( PATHS.build ));
});
// **********************************************************************

gulp.task('minify:css', () => {
  return gulp.src( PATHS.build +'/styles/*.css')
    .pipe($.cache($.postcss([cssnano({autoprefixer: false})])))
    .pipe(gulp.dest( PATHS.build +'/styles' ));
});

gulp.task('minify:js', () => {
  return gulp.src( PATHS.build +'/scripts/*.js')
    .pipe($.cache($.uglify()))
    .pipe(gulp.dest( PATHS.build +'/scripts' ));
});

gulp.task('minify', ['minify:css','minify:js'], () => {
  return gulp.src( PATHS.build +'/**/*');
});
// **********************************************************************

gulp.task('clean', del.bind(null, [ PATHS.temp, PATHS.build ]));

gulp.task('serve', ['views', 'styles', 'scripts', 'fonts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: [ PATHS.temp, PATHS.src ],
      routes: {
        '/node_modules': 'node_modules',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch([
    PATHS.src +'/scripts/**/*.js',
    PATHS.src +'/images/**/*',
    PATHS.temp +'/fonts/**/*',
    PATHS.temp +'/**/*.html',
  ]).on('change', reload);

  gulp.watch([PATHS.src +'/styles/**/*.scss', PATHS.src +'/components/**/*.scss'], ['styles']);
  gulp.watch([PATHS.src +'/scripts/**/*.js',PATHS.src +'/components/**/*.js'], ['scripts']);
  gulp.watch( PATHS.src +'/fonts/**/*', ['fonts']);
  gulp.watch([PATHS.src +'/views/**/*.html',PATHS.src +'/components/**/*.html'], ['views']);
});

gulp.task('build', ['html', 'images', 'samples', 'extras', 'fonts'], () => {
  gulp.start('minify');
});

gulp.task('default', ['clean'], () => {
  gulp.start('build');
});
