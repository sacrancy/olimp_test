(function() {
  $(document).ready(function() {
    $('#phone-input-country').inputmask('(+9)');
    $('#phone-input').inputmask('999-99-99');
    $('select').niceSelect();
  });
})();